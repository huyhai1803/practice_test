/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Middleware;

import Middleware.thrift.MiddlewareService;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TServer.Args;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;

public class MiddlewareServer {

    public static MiddlewareHandler handler;

    public static MiddlewareService.Processor processor;
    public static void main(String [] args) {
        try {
            // Create handler using MiddlewareHandler class defined
            handler = new MiddlewareHandler();
            // Create processor for server using handler
            processor = new MiddlewareService.Processor(handler);

            // Run thrift processor for server 
            Runnable middleware = new Runnable() {
                public void run() {
                    middleware_thrift(processor);
                }
            };
            new Thread(middleware).start();
            } catch (Exception x) {
                x.printStackTrace();
            }
    }

    public static void middleware_thrift(MiddlewareService.Processor processor) {
        try {
            // Create TServerSocket with port 8081
            TServerTransport serverTransport = new TServerSocket(8081);
            TServer server = new TSimpleServer(new Args(serverTransport).processor(processor));

            System.out.println("Starting the middleware server...");
            server.serve();
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
}
