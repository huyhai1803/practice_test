/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Middleware;

import Middleware.thrift.MiddlewareService;
import org.apache.thrift.TException;

public class MiddlewareHandler implements MiddlewareService.Iface {
    /*
     * Override the method of thrift interface using method in Model class defined
    */
    @Override
    public int nextPrimeNumber(int x) throws TException {
        Model m = Model.getInstance();
        return m.nextPrimeNumber(x);
    }
    
    @Override
    public void putToDatabase(java.lang.String id, java.lang.String Name, java.lang.String Gender, java.lang.String Age) {
        Model m = Model.getInstance();
        m.putToDatabase(id, Name, Gender, Age);
    }
    
    @Override
    public String getFromDatabase(java.lang.String id) {
        Model m = Model.getInstance();
        String info = m.getFromDatabase(id);
        return info;
    }
}