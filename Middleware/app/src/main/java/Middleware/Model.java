/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Middleware;

import java.lang.Math;
import java.util.Map;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisException;
import redis.clients.jedis.Transaction;

/**
 *
 * @author lap15374
 */
public class Model {
    private static Model model_instance = null;
    
    //address of your redis server
    private static final String redisHost = "localhost";
    private static final Integer redisPort = 6379;
    
    //the jedis connection pool..
    private static JedisPool pool = null;
    
    private Model() {
        
    }
    
    // Function to check if a number is prime number
    public boolean checkPrime(int x) {
        if (x < 2)
            return false;
        for (int i=2; i<=Math.floor(Math.sqrt(x)); i++) {
            if (x % i == 0)
                return false;
        }
        return true;
    }
    
    // Function to find the next prime number in range (1, 1000000)
    public int nextPrimeNumber(int x) {
        if (x <= 0 || x >= 1000000)
            return -1;
        while (!checkPrime(x))
            x += 1;
        return x;
    }
    
    public void putToDatabase(String id, String Name, String Gender, String Age) {
        //configure our pool connection
        pool = new JedisPool(redisHost, redisPort);
        
        //get a jedis connection jedis connection pool
        Jedis jedis = pool.getResource();
        
        try {
            //save to redis
            Transaction t = jedis.multi();
            t.del(id);
            t.hset(id, "Name", Name);
            t.hset(id, "Gender", Gender);
            t.hset(id, "Age", Age);
            t.exec();
            
            System.out.println("Successfully put data of user with id " + id);
        } catch (JedisException e) {
            //if something wrong happen, return it back to the pool
            if (null != jedis) {
                pool.returnBrokenResource(jedis);
                jedis = null;
            }
        } finally {
            ///it's important to return the Jedis instance to the pool once you've finished using it
            if (null != jedis)
                pool.returnResource(jedis);
        }
    }
    
    public String getFromDatabase(String id) {
        //configure our pool connection
        pool = new JedisPool(redisHost, redisPort);
        
        //get a jedis connection jedis connection pool
        Jedis jedis = pool.getResource();
        
        String info = "";
        
        try {

            //read from redis
            Map<String, String> fields = jedis.hgetAll(id);
            info = "id: " + id + ", Name: " + fields.get("Name") 
                               + ", Gender: " + fields.get("Gender") 
                               + ", Age: " + fields.get("Age");
        } catch (JedisException e) {
            //if something wrong happen, return it back to the pool
            if (null != jedis) {
                pool.returnBrokenResource(jedis);
                jedis = null;
            }
        } finally {
            ///it's important to return the Jedis instance to the pool once you've finished using it
            if (null != jedis)
                pool.returnResource(jedis);
        }
        
        return info;
    }
    
    public static Model getInstance() {
        // Singleton design pattern
        if (model_instance == null)
            model_instance = new Model();
        
        return model_instance;
    }
}
