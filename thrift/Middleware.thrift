namespace java Middleware
namespace py Middleware
typedef i32 int // We can use typedef to get pretty names for the types we are using

service MiddlewareService
{
int nextPrimeNumber(1:int x),
void putToDatabase(1:string id, 2:string Name, 3:string Gender, 4:string Age),
string getFromDatabase(1:string id),
}
